$(function () {
    //############## GOTO CORE
    $('.wc_goto').click(function () {
        var Goto = $($(this).attr("href"));
        if (Goto.length) {
            $('html, body').animate({scrollTop: Goto.offset().top}, 800);
        } else {
            $('html, body').animate({scrollTop: 0}, 800);
        }
        return false;
    });
});


$gn.ready(function (variable) {    
    var payment_forms = ["credit_card", "banking_billet"];
    variable.lightbox(payment_forms);

    variable.jq('#button_lightbox').click(function (e) {
        var Price = $(this).attr('data-price');
        e.preventDefault();
        e.stopPropagation();

        var data = {
            items: [{
                    code: 1,
                    name: 'Run Fitness E-commerce',
                    value: parseInt(Price)
                }],
            shippingAddress: false,
            actionForm: './_ajax/callback.ajax.php'
        };

        variable.show(data);
    });

});

var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/571b9f1146e2f0b10424d3de/1atkuc7bs';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();